# Villiers Park 2015
# For Villiers Park 2015 project virtual machines

import curses
import subprocess
import argparse

# When SystemD calls us
def boot(scr):
    subprocess.call(["/bin/systemctl", "stop", "gdm.service"])
    subprocess.call(["/bin/chvt", "2"])
    scr.getch()
    subprocess.call(["/bin/chvt", "1"])
    subprocess.call(["/bin/systemctl", "start", "gdm.service"])

# When ran from command line
def console(scr):
    pass

